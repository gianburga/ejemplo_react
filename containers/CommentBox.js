// CommentBox.js
import {connect} from 'react-redux'
import CommentApp from '../components/CommentApp'
import { addComment, deleteComment, cleanedForm } from '../actions';


const mapStateToProps = (state) => {
	return state
}

const mapDispatchToProps = (dispatch) => {
	return {
		addComment: (comment) => {
			dispatch(addComment(comment))
		},
		deleteComment: (index) => {
			dispatch(deleteComment(index))
		},
		cleanedForm: () => dispatch(cleanedForm())
	}
}

const CommentBox = connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentApp)

export default CommentBox
