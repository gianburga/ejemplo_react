// store/index.js

import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'
import { selectSubreddit, fetchPosts } from '../actions'
import rootReducer from '../reducers'

const loggerMiddleware = createLogger()

export default function(initialState){
	return createStore(
	  rootReducer,
	  initialState,
	  applyMiddleware(
	    thunkMiddleware, // lets us dispatch() functions
	    loggerMiddleware // neat middleware that logs actions
	  )
	)
}
