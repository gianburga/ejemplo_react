// index.js


export const DISABLE_FORM = 'DISABLE_FORM'
export const ENABLE_FORM = 'ENABLE_FORM'
function disableForm(disable){
	return {
		type: disable ? DISABLE_FORM : ENABLE_FORM
	}
}

export const CLEANED_FORM = 'CLEANED_FORM'
export function cleanedForm(){
	return {
		type: CLEANED_FORM
	}
}

export const CLEAN_FORM = 'CLEAN_FORM'
function cleanForm(){
	return {
		type: CLEAN_FORM
	}
}


function postComment(commentText){
	return new Promise(function(resolve, reject){
		var errors = {
			comentarios: []
		};

		if (!commentText.trim()) {
			errors.comentarios.push('Tienes que escribir un comentario.')
		}

		if (commentText.length < 3){
			errors.comentarios.push('El comentario es demasiado corto.')
		}
		
		if (buscarLisuras(commentText)){
			errors.comentarios.push('El comentario no puede contener lisuras.')
		}

		if (errors.comentarios.length){
			reject({
				status: 400,
				json: {
					"comentario": errors.comentarios
				}
			})
		} else {
			setTimeout(function(){
				resolve('{\
					"comentario": "' + commentText + '",\
					"usuario": {\
						"name": "Gian Burga"\
					}\
				}')
			}, 3000)
		}
	})
}

export const REQUEST_ADD_COMMENT = 'REQUEST_ADD_COMMENT'
function requestAddComment(){
	return {
		type: REQUEST_ADD_COMMENT,
	}
}

export const RECEIVE_ADDED_COMMENT = 'RECEIVE_ADDED_COMMENT'
function receiveAddedComment(comment){
	return {
		type: RECEIVE_ADDED_COMMENT,
		comment
	}
}

export const ERROR_ADD_COMMENT = 'ERROR_ADD_COMMENT'
function errorAddComment(errors){
	return {
		type: ERROR_ADD_COMMENT,
		errors
	}
}

function handleValidationError(callback){
	return function(response){
		switch (response.status) {
			case 400:
				var errors = [];
				var errorObject = response.json;
				for (key in errorObject) {
					if (errorObject.hasOwnProperty(key)) {
						for (var i = errorObject[key].length - 1; i >= 0; i--) {
							errors.push(key + ': ' + errorObject[key][i])
						}
					}
				}
				callback({
					errors: errors
				})
				break;
			default:
				// statements_def
				break;
		}
	}
}

function parseJSON(text){
	return JSON.parse(text)
}

export const ADD_COMMENT = 'ADD_COMMENT'
export function addComment(comment){
	return function(dispatch){
		dispatch(disableForm(true))
		dispatch(requestAddComment())

		postComment(comment)
			.then(parseJSON)
			.then(comment => {
				dispatch(cleanForm())
				dispatch(disableForm(false))
				dispatch(receiveAddedComment(comment))
			})
			.catch(handleValidationError(({errors}) => {
				dispatch(disableForm(false))
				dispatch(errorAddComment(errors))
			}))
	}	
}

export const DELETE_COMMENT = 'DELETE_COMMENT'
export function deleteComment(index){
	return function(dispatch, getState){
		let comments = getState().comments
		let deletedComment = comments[index]

		delete comments[index]

		dispatch({
			type: DELETE_COMMENT,
			deletedComment,
			index
		})
	}
}

function buscarLisuras(text){
	var lisuras = ["mierda", "puta", "puto", "juan"];
	for(var i = 0, length1 = lisuras.length; i < length1; i++){
		if (text.indexOf(lisuras[i]) != -1){
			return true;
		}
	}
	return false;
}

// module.exports = {
// 	deleteComment: function(index){},
// 	addComment: function(commentText){}
// }