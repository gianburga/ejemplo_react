// reducers/index.js
import { 
	DELETE_COMMENT,
	REQUEST_ADD_COMMENT, 
	RECEIVE_ADDED_COMMENT,
	ERROR_ADD_COMMENT,
	ENABLE_FORM, DISABLE_FORM, 
	CLEAN_FORM, CLEANED_FORM
} from '../actions'
import comments from '../components/comments'

function CommentApp(state = {
	isLoading: false,
	comments: comments,
	errors: [],
	resetForm: false,
	disableForm: false
}, action){
	switch (action.type) {
		case ENABLE_FORM: 
			return Object.assign({}, state, {
				disableForm: false
			})
		case DISABLE_FORM: 
			return Object.assign({}, state, {
				disableForm: true
			})
		case CLEANED_FORM:
			return Object.assign({}, state, {
				resetForm: false,
			})
		case CLEAN_FORM: 
			return Object.assign({}, state, {
				resetForm: true,
			})
		case DELETE_COMMENT:
			let copyComments = state.comments.slice();
			delete copyComments[action.index]

			return Object.assign({}, state, {
				comments: copyComments
			})
		case REQUEST_ADD_COMMENT:
			return Object.assign({}, state, {
				isLoading: true
			})
		case RECEIVE_ADDED_COMMENT:
			return Object.assign({}, state, {
				isLoading: false,
				comments: [
					...state.comments,
					action.comment
				]
			})
		case ERROR_ADD_COMMENT:
			return Object.assign({}, state, {
				isLoading: false,
				errors: action.errors
			})
		default:
			return state
	}
}

export default CommentApp
