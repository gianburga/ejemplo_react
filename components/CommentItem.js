// CommentItem.js
var React = require('react');

module.exports = ({ text, author, onDeleteComment }) => (
	<li>
		{text}, {author},&nbsp; 
		<a href="#" onClick={onDeleteComment}>X</a>
	</li>
)