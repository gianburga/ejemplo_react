// CommentForm.js
var React = require('react');

module.exports = React.createClass({
	render: function(){
		var errors = this.props.errors.map(function(errorMessage, index){
			return (<li key={index}>{errorMessage}</li>)
		})

		return (
			<form>
				<ul className="list-errors">{errors}</ul>
				<textarea 
					disabled={this.props.disabled}
					placeholder="Agregar un comentario" 
					ref="input_comentario"/>
				<button 
					disabled={this.props.disabled}
					ref="button_comentario" 
					onClick={this.props.onComment}>Comentar</button>
			</form>
		)
	}
})
