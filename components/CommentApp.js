var React = require('react');
var CommentList = require('./CommentList');
var CommentForm = require('./CommentForm');
var comentarios = require('./comments');


module.exports = React.createClass({
	componentDidMount: function(){},
	componentWillReceiveProps: function(nextProps){
		var commentForm = this.refs.comment_form;
		var inputComment = commentForm.refs.input_comentario;
		if (nextProps.resetForm){
			inputComment.value = ''
			this.props.cleanedForm()
		}
	},
	onComment: function(e){
		e.preventDefault();

		var commentForm = this.refs.comment_form;
		var inputComment = commentForm.refs.input_comentario;
		var buttonComment = commentForm.refs.button_comentario;

		this.props.addComment(inputComment.value)
	},
	onDeleteComment: function(index){
		this.props.deleteComment(index)
	},
	render: function(){
		return (
			<div>
				<h1>Comentarios</h1>
				<h3>{this.props.isLoading ? 'Comentando...' : ''}</h3>	
				<CommentForm 
					ref="comment_form"
					disabled={this.props.disableForm}
					errors={this.props.errors}
					onComment={this.onComment}/>
				<CommentList 
					comments={this.props.comments}
					onDeleteComment={this.onDeleteComment} />
			</div>
		)
	}
})
