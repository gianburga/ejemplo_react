// CommentList.js
var React = require('react');
var CommentItem = require('./CommentItem');

module.exports = React.createClass({
	render: function () {
		var props = this.props;
		return (
			<ul>
				{props.comments.map(function(comment, index){
					if (comment == undefined) return;
					return (
						<CommentItem 
							key={index}
							text={comment.comentario} 
							author={comment.usuario.name} 
							onDeleteComment={function(e){
								e.preventDefault();
								props.onDeleteComment(index);
							}} />)
				})}
			</ul>)
	}
})